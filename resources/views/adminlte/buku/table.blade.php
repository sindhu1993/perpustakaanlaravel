@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Buku</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Buku</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Buku</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-md btn-primary mb-3" onClick="showModalAdd()">Input Buku</button>
            </div>
        </div>
      <table id="buku" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Kategori</th>
          <th>Cover Buku</th>
          <th>Judul</th>
          <th>Pengarang</th>
          <th>Tahun Terbit</th>
          <th>Stok Buku</th>
          <th>Tempat Simpan</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
            @if (count($buku)>0)
            @foreach ($buku as $k => $v)
                <tr>
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->nama_kategori }}</td>
                    <td><img src="{{ asset('uploads/buku/'.$v->cover_buku)}}" style="width: 100px;height:100px;"></td>
                    <td>{{ $v->judul }}</td>
                    <td>{{ $v->pengarang }}</td>
                    <td>{{ $v->tahun_terbit }}</td>
                    <td>{{ $v->stok_buku }}</td>
                    <td>{{ $v->tempat_simpan }}</td>
                    <td>
                        <a href="/admin/buku/edit/{{ $v->id }}" class="btn btn-sm btn-warning">Edit</a>
                            <form action="{{ route('admin/buku/prosesDelete') }}" method="POST">
                                @csrf
                                <input type="hidden" value="{{ $v->id }}" name="id">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                    </td>
                </tr>
            @endforeach
            @else
            <tr>
                <td colspan="9" align="center">No Data</td>
            </tr>
            @endif
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>

  {{-- modal add --}}
  <div class="modal fade" id="modalAdd">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Form Add Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('/admin/buku') }}" method="POST"  enctype="multipart/form-data">
                @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Cover Buku</label>
                        <input type="file" readonly class="form-control" name="cover_buku" autocomplete="off">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Kategori</label>
                      <select class="form-control select2bs4" name="id_kategori" style="width: 100%;">
                          <option value="default">Pilih Kategori</option>
                          @foreach ($kategori as $k => $v)
                              <option value="{{ $v->id }}">{{ $v->nama_kategori }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Judul</label>
                        <input type="text" class="form-control" class="form-control" autocomplete="off"
                        placeholder="Judul"
                        name="judul">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Pengarang</label>
                        <input type="text" class="form-control" name="pengarang"
                        placeholder="Pengarang" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Tahun Terbit</label>
                    <input type="text" class="form-control" name="tahun_terbit" id="tahun_terbit"
                    placeholder="Tahun terbit" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Stok</label>
                    <input type="number" class="form-control" name="stok_buku"
                    placeholder="Stok Buku" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tempat Simpan</label>
                  <input type="text" class="form-control" placeholder="Tempat Simpan"
                  name="tempat_simpan" autocomplete="off">
              </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Simpan</button>
        </div>
    </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  {{-- modal add --}}

  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>
<script>
  $(function () {
    $("#buku").DataTable();
  });

    //Initialize Select2 Elements
    $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });

    $('#tahun_terbit').datepicker({
    todayHighlight:'TRUE',
    autoclose: true,
    format: 'yyyy',
    showButtonPanel: true,
    viewMode: "years",
        minViewMode: "years"
    });

  function showModalAdd() {
    $('#modalAdd').modal('show');
  }
</script>
@endpush
