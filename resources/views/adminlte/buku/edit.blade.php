@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>CRUD BUKU</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Buku</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Edit BUKU</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-md btn-primary mb-3" href="{{ route('/admin/buku') }}">Input Buku</a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                {{--  --}}
                <form action="{{ route('/admin/buku/edit') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Cover Buku</label>
                                <input type="file" readonly class="form-control" name="cover_buku" autocomplete="off">
                                <input type="text" value="{{ $buku[0]->cover_buku }}" class="form-control" name="old_image" autocomplete="off">
                                <input type="text" value="{{ $buku[0]->id }}" class="form-control" name="id" autocomplete="off">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Kategori</label>
                              <select class="form-control select2bs4" name="id_kategori" style="width: 100%;">
                                  <option value="default">Pilih Kategori</option>
                                  @foreach ($kategori as $k => $v)
                                        @if ($buku[0]->id_kategori == $v->id)
                                        <option value="{{ $v->id }}" selected>{{ $v->nama_kategori }}</option>
                                        @else
                                        <option value="{{ $v->id }}">{{ $v->nama_kategori }}</option>
                                        @endif
                                  @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Judul</label>
                                <input type="text" class="form-control" class="form-control" autocomplete="off"
                                placeholder="Judul" value="{{ $buku[0]->judul }}"
                                name="judul">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Pengarang</label>
                                <input type="text" class="form-control" name="pengarang" value="{{ $buku[0]->pengarang }}"
                                placeholder="Pengarang" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputPassword1">Tahun Terbit</label>
                            <input type="text" class="form-control" name="tahun_terbit" id="tahun_terbit" value="{{ $buku[0]->tahun_terbit }}"
                            placeholder="Tahun terbit" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Stok</label>
                            <input type="number" class="form-control" name="stok_buku" value="{{ $buku[0]->stok_buku }}"
                            placeholder="Stok Buku" autocomplete="off">
                        </div>
                        <div class="form-group">
                          <label for="exampleInputPassword1">Tempat Simpan</label>
                          <input type="text" class="form-control" placeholder="Tempat Simpan" value="{{ $buku[0]->tempat_simpan }}"
                          name="tempat_simpan" autocomplete="off">
                      </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success" id="proses">Update Data</button>
            </div>
        </form>
                {{--  --}}
            </div>
        </div>

    </div>
    <!-- /.card-body -->
  </div>

  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>

<script>

    $('#tahun_terbit').datepicker({
    todayHighlight:'TRUE',
    autoclose: true,
    format: 'yyyy',
    showButtonPanel: true,
    viewMode: "years",
        minViewMode: "years"
    });

      //Initialize Select2 Elements
      $('.select2').select2();

      //Initialize Select2 Elements
      $('.select2bs4').select2({
          theme: 'bootstrap4'
      });

  </script>
@endpush
