@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>CRUD Kategori</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Data Kategori</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Kategori</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-md btn-primary mb-3" onClick="showModalAdd()">Add Data</button>
            </div>
        </div>
      <table id="kategori" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
            @if (count($kategori_buku)>0)
            @foreach ($kategori_buku as $k => $v)
                <tr>
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->nama_kategori }}</td>
                    <td>
                        <button onClick="showModalEdit({{ $v->id }},`{{ $v->nama_kategori }}`)"
                            class="btn btn-sm btn-warning">Edit</button>
                            <form action="{{ route('admin/kategori/prosesDelete') }}" method="POST">
                                @csrf
                                <input type="hidden" value="{{ $v->id }}" name="id">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                    </td>
                    {{-- <td>
                        <form action="{{ route('admin/prosesDeleteTempatKerja') }}" method="POST">
                            @csrf --}}
                            {{-- <input type="hidden" value="{{ $v->id_status_tempat_kerja }}" name="id_status_tempat_kerja">
                            <a href="{{ route('admin/edit/{slug_tempat_kerja}',$v->slug_tempat_kerja)}}" class="btn btn-warning btn-md">Edit</a>
                            <button type="submit" class="btn btn-danger btn-md">Delete</button>
                        </form>

                    </td> --}}
                </tr>
            @endforeach
            @else
            <tr>
                <td colspan="4" align="center">No Data</td>
            </tr>
            @endif
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>

  {{-- modal add --}}
  <div class="modal fade" id="modalAdd">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Form Add Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('/admin/kategori') }}" method="POST">
                @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama Kategori</label>
                        <input type="text" class="form-control" name="nama_kategori" placeholder="Nama" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Tambahkan</button>
        </div>
    </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  {{-- modal add --}}

  {{-- modal edit --}}
  <div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Form Edit Data</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('/admin/kategori/edit') }}" method="POST">
                @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="hidden" id="id" name="id">
                        <label for="exampleInputEmail1">Nama Kategori</label>
                        <input type="text" class="form-control" name="nama_kategori" id="nama_kategori" placeholder="Nama" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success" id="proses">Tambahkan</button>
        </div>
    </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  {{-- modal edit --}}

  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>
<script>
  $(function () {
    $("#kategori").DataTable();
  });

  function showModalAdd() {
    $('#modalAdd').modal('show');
  }

  function showModalEdit(id,nama)
  {
    $('#nama_kategori').val(nama);
    $('#id').val(id);
    $('#proses').text('Update');
    $('#modalEdit').modal('show');
  }
</script>
@endpush
