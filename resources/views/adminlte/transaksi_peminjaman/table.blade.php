@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Peminjaman</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Peminjaman</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Transaksi Peminjaman</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-md btn-primary mb-3" href="{{ route('/admin/tambahpinjam/') }}">Input Peminjaman</a>
            </div>
        </div>
      <table id="pinjam" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Kode Trans</th>
          <th>Judul</th>
          <th>Jumlah Buku</th>
          <th>Tgl Pinjam</th>
          <th>Nama Peminjam</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
            @if (count($pinjam)>0)
            @foreach ($pinjam as $k => $v)
                <tr>
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->kode_trans }}</td>
                    <td>{{ $v->judul }}</td>
                    <td>{{ $v->jumlah_buku }}</td>
                    <td>
                      <?php $x = $v->tgl_pinjam;
                      echo date("d-m-Y", strtotime($x));
                      ?></td>
                    <td>{{ $v->nama_peminjam }}</td>
                    <td>
                        <a href="/admin/pinjam/edit/{{ $v->id_transaksi_peminjaman }}" class="btn btn-sm btn-warning">Edit</a>
                            <form action="{{ route('admin/pinjam/prosesDelete') }}" method="POST">
                                @csrf
                                <input type="hidden" value="{{ $v->id_transaksi_peminjaman }}" name="id_transaksi_peminjaman">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                    </td>
                </tr>
            @endforeach
            @else
            <tr>
                <td colspan="9" align="center">No Data</td>
            </tr>
            @endif
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>


  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>
<script>
  $(function () {
    $("#pinjam").DataTable();
  });

    //Initialize Select2 Elements
    $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });

    $('#tahun_terbit').datepicker({
    todayHighlight:'TRUE',
    autoclose: true,
    format: 'yyyy',
    showButtonPanel: true,
    viewMode: "years",
        minViewMode: "years"
    });

</script>
@endpush
