@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>CRUD Transaksi Peminjaman</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Peminjaman</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Edit Peminjaman</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                {{--  --}}
                <form action="{{ route('/admin/pinjam/prosesEdit') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Kode Trans</label>
                                <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                placeholder="Kode Trans" value="{{ $pinjam[0]->kode_trans }}" readonly
                                name="kode_trans">
                                <input type="hidden" readonly class="form-control" class="form-control" autocomplete="off"
                                placeholder="Kode Trans" value="{{ $pinjam[0]->id_transaksi_peminjaman }}" readonly
                                name="id_transaksi_peminjaman">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Buku</label>
                                <select class="form-control select2bs4" name="id_buku" style="width: 100%;">
                                    <option value="default">Pilih Buku</option>
                                    @foreach ($buku as $k => $v)
                                            @if ($pinjam[0]->id_buku == $v->id)
                                            <option value="{{ $v->id }}" selected>{{ $v->judul }}</option>
                                            @else
                                            <option value="{{ $v->id }}">{{ $v->judul }}</option>
                                            @endif
                                    @endforeach
                                  </select>
                              </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Peminjam</label>
                              <select class="form-control select2bs4" name="id_peminjam" style="width: 100%;">
                                  <option value="default">Pilih Peminjam</option>
                                  @foreach ($peminjam as $k => $v)
                                        @if ($pinjam[0]->id_peminjam == $v->id)
                                        <option value="{{ $v->id }}" selected>{{ $v->nama_peminjam }}</option>
                                        @else
                                        <option value="{{ $v->id }}">{{ $v->nama_peminjam }}</option>
                                        @endif
                                  @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Jumlah Buku</label>
                                <input type="number" class="form-control" name="jumlah_buku" min="1"
                                placeholder="Jumlah Buku" autocomplete="off" value="{{ $pinjam[0]->jumlah_buku }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tgl Pinjam</label>
                                <input type="text" class="form-control" class="form-control" autocomplete="off" value="{{ $pinjam[0]->tgl_pinjam }}"
                                placeholder="Tgl Pinjam" id="tgl_pinjam"
                                name="tgl_pinjam">
                            </div>
                        </div>
                    </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success" id="proses">Update</button>
            </div>
        </form>
                {{--  --}}
            </div>
        </div>

    </div>
    <!-- /.card-body -->
  </div>

  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>

<script>

$('#tgl_pinjam').datepicker({
    todayHighlight:'TRUE',
    autoclose: true,
	format: 'dd-mm-yyyy',
	showButtonPanel: true
}).change(function() {
	var date1 =   $('#tgl_pinjam').datepicker({ dateFormat: 'dd-mm-yy' }).val();
	$('#tgl_pinjam').val(date1);
});

      //Initialize Select2 Elements
      $('.select2').select2();

      //Initialize Select2 Elements
      $('.select2bs4').select2({
          theme: 'bootstrap4'
      });

  </script>
@endpush
