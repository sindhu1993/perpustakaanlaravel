@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Transaksi Pengembalian</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Pengembalian</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Pengembalian</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                {{--  --}}
                {{-- <form action="{{ route('/admin/kembali/cari') }}" method="POST" enctype="multipart/form-data"> --}}
                    {{-- @csrf --}}
                    <input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control" name="kode_trans" id="kode_trans"
                                placeholder="Kode Trans">
                                <span class="input-group-append">
                                  &nbsp;&nbsp;<button type="button" onClick="Cari()" class="btn btn-info btn-flat">Cari</button>
                                </span>
                              </div>
                        </div>
                    </div>
            </div>
        {{-- </form> --}}
                {{--  --}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mx-3" id="show_data_kembali">

            </div>
        </div>

    </div>
    <!-- /.card-body -->
  </div>

  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>

<script>



      //Initialize Select2 Elements
      $('.select2').select2();

      //Initialize Select2 Elements
      $('.select2bs4').select2({
          theme: 'bootstrap4'
      });


      function Cari()
      {
        var kode_trans = $('#kode_trans').val();
        $.ajax({
            'url':'/admin/kembali/cari',
            'type':'POST',
            'data':{
                kode_trans : kode_trans,
                _token:$('#csrf').val()
            },
            success:function(data){
                var res = JSON.parse(data);
                // console.log(res);
                if(res.status == 9 || res.status == "9")
                {
                    Swal.fire({
                    icon: 'warning',
                    title: 'Warning',
                    text: 'Data Tidak Ditemukan',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                    });
                }
                else
                {
                    if(res.status == 1 || res.status == "1")
                    {
                        var html = `
                        <form method="POST" action={{ route('/admin/kembali/prosesKembali') }}>
                        @csrf
                            <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="hidden" value="${res.data[0].id_buku}" name="id_buku">
                                        <label for="exampleInputPassword1">Kode Trans</label>
                                        <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                        placeholder="Kode Trans" value="${res.data[0].kode_trans}" id="pkode_trans"
                                        name="kode_trans">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Buku</label>
                                        <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                        placeholder="Kode Trans" value="${res.data[0].judul}">
                                    </div>
                                    <div class="form-group">
                                    <label for="exampleInputPassword1">Peminjam</label>
                                    <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                        placeholder="Kode Trans" value="${res.data[0].nama_peminjam}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Denda</label>
                                        <input type="number" class="form-control" class="form-control" autocomplete="off"
                                        placeholder="Denda" id="denda"
                                        name="denda">
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jumlah Buku</label>
                                        <input type="number" class="form-control" name="jumlah_buku" value="${res.data[0].jumlah_buku}"
                                        readonly
                                        placeholder="Jumlah Buku" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tgl Pinjam</label>
                                        <input type="text" class="form-control" class="form-control" autocomplete="off"
                                        placeholder="Tgl Pinjam" id="tgl_pinjam" value="${res.data[0].tgl_pinjam}" readonly
                                        name="tgl_pinjam">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tgl Kembali</label>
                                        <input type="text" class="form-control" class="form-control" autocomplete="off"
                                        placeholder="Tgl Kembali" id="tgl_kembali" required
                                        name="tgl_kembali">
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-info">Kembalikan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        `;
                        $('#show_data_kembali').html(html);

                        $('#tgl_kembali').datepicker({
                            todayHighlight:'TRUE',
                            autoclose: true,
                            format: 'dd-mm-yyyy',
                            showButtonPanel: true
                        }).change(function() {
                            var date1 =   $('#tgl_kembali').datepicker({ dateFormat: 'dd-mm-yy' }).val();
                            $('#tgl_kembali').val(date1);
                        });
                    }
                }
            }
        });
      }

    function ProsesKembali()
    {
        var kode_trans = $('#pkode_trans').val();
        $.ajax({
            'url':'/admin/kembali/prosesKembali',
            'type':'POST',
            'data':{
                kode_trans : kode_trans,
                _token:$('#csrf_token').val(),
                tgl_kembali : tgl_kembali,
                denda : denda
            },
            success:function(data){
                var res = JSON.parse(data);
                // console.log(res);
                if(res.status == 9 || res.status == "9")
                {
                    Swal.fire({
                    icon: 'warning',
                    title: 'Warning',
                    text: 'Gagal Di kembalikan',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                    });
                }
                else
                {
                    if(res.status == 1 || res.status == "1")
                    {
                        Swal.fire({
                        icon: 'warning',
                        title: 'Warning',
                        text: 'Berhasil Di kembalikan',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                        }).then((result) => {
                            if (result.value) {
                                window.location.href = '/admin/kembali';
                            }
                        });
                    }
                }
            }
        });
    }

  </script>
@endpush
