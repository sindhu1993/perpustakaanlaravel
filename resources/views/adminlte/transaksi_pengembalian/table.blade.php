@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pengembalian</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Pengembalian</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Transaksi Pengembalian</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-md btn-primary mb-3" href="{{ route('/admin/tambahkembali/') }}">Input Pengembalian</a>
            </div>
        </div>
      <table id="kembali" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Kode Trans</th>
          <th>Judul</th>
          <th>Jumlah Buku</th>
          <th>Tgl kembali</th>
          <th>Nama Peminjam</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
            @if (count($kembali)>0)
            @foreach ($kembali as $k => $v)
                <tr>
                    <td>{{ $k + 1 }}</td>
                    <td>{{ $v->kode_trans }}</td>
                    <td>{{ $v->judul }}</td>
                    <td>{{ $v->jumlah_buku }}</td>
                    <td>
                      <?php 
                        $x = $v->tgl_kembali;
                        echo date("d-m-Y", strtotime($x));?></td>
                    <td>{{ $v->nama_peminjam }}</td>
                    <td>
                        <a href="/admin/kembali/edit/{{ $v->id_transaksi_pengembalian }}" class="btn btn-sm btn-warning">Edit</a>
                            {{-- <form action="{{ route('admin/kembali/prosesDelete') }}" method="POST">
                                @csrf
                                <input type="hidden" value="{{ $v->id_transaksi_pengembalian }}" name="id_transaksi_Pengembalian">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form> --}}
                    </td>
                </tr>
            @endforeach
            @else
            <tr>
                <td colspan="9" align="center">No Data</td>
            </tr>
            @endif
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>


  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>
<script>
  $(function () {
    $("#kembali").DataTable();
  });

    //Initialize Select2 Elements
    $('.select2').select2();

    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });

    $('#tahun_terbit').datepicker({
    todayHighlight:'TRUE',
    autoclose: true,
    format: 'yyyy',
    showButtonPanel: true,
    viewMode: "years",
        minViewMode: "years"
    });

</script>
@endpush
