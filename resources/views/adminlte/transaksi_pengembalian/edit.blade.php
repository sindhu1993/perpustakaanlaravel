@extends('adminlte.master')

@section('content')


<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Transaksi Pengembalian</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Pengembalian</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <section class="content">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Edit Pengembalian</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-12 mb-4">
                @if (Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @endif

                @if (Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session('error') }}
                    </div>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mx-3">
                <form method="POST" action={{ route('/admin/kembali/prosesEdit') }}>
                    @csrf
                        <div class="col-md-12">
                                <div class="form-group">
                                    <input type="hidden" value="{{ $kembali[0]->id_buku }}" name="id_buku">
                                    <input type="hidden" value="{{ $kembali[0]->id_transaksi_pengembalian }}" name="id_transaksi_pengembalian">
                                    <label for="exampleInputPassword1">Kode Trans</label>
                                    <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                    placeholder="Kode Trans" value="{{ $kembali[0]->kode_trans }}" id="pkode_trans"
                                    name="kode_trans">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Buku</label>
                                    <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                    placeholder="Kode Trans" value="{{ $kembali[0]->judul }}">
                                </div>
                                <div class="form-group">
                                <label for="exampleInputPassword1">Peminjam</label>
                                <input type="text" readonly class="form-control" class="form-control" autocomplete="off"
                                    placeholder="Kode Trans" value="{{ $kembali[0]->nama_peminjam }}">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Denda</label>
                                    <input type="number" class="form-control" class="form-control" autocomplete="off"
                                    placeholder="Denda" id="denda" value="{{ $kembali[0]->denda }}"
                                    name="denda">
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Jumlah Buku</label>
                                    <input type="number" class="form-control" name="jumlah_buku" value="{{ $kembali[0]->jumlah_buku }}"
                                    readonly
                                    placeholder="Jumlah Buku" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Tgl Kembali</label>
                                    <input type="text" class="form-control" class="form-control" autocomplete="off"
                                    placeholder="Tgl Kembali" id="tgl_kembali" required value="{{ $kembali[0]->tgl_kembali }}"
                                    name="tgl_kembali">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Kembalikan</button>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>

    </div>
    <!-- /.card-body -->
  </div>

  </section>
@endsection


@push('scripts')

<script src="{{ asset('backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script src="{{ asset('backend/dist/js/swal.min.js')}}"></script>

<script>

$('#tgl_kembali').datepicker({
    todayHighlight:'TRUE',
    autoclose: true,
	format: 'dd-mm-yyyy',
	showButtonPanel: true
}).change(function() {
	var date1 =   $('#tgl_kembali').datepicker({ dateFormat: 'dd-mm-yy' }).val();
	$('#tgl_kembali').val(date1);
});

      //Initialize Select2 Elements
      $('.select2').select2();

      //Initialize Select2 Elements
      $('.select2bs4').select2({
          theme: 'bootstrap4'
      });

  </script>
@endpush
