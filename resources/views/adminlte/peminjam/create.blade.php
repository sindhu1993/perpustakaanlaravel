@extends('adminlte.master')

@section('title')
Tambah Peminjam
@endsection

@section('content')
<div>
        <form action="/admin/peminjam/create" method="POST">
            @csrf
            <div class="form-group">
                <label for ="title">Nama</label>
                <input type="text" class="form-control" name="nama_peminjam" id="title" placeholder="Masukan Nama">
                @error('nama_peminjam')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for ="title">Nomor HP</label>
                <input type="text" class="form-control" name="no_hp_peminjam" id="body" placeholder="Masukan Nomor HP">
                @error('no_hp_peminjam')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for ="title">Email</label>
                <input type="text" class="form-control" name="email_peminjam" id="body" placeholder="Masukan Email">
                @error('email_peminjam')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>
@endsection