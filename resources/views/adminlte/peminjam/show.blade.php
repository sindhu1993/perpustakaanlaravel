@extends('adminlte.master')

@section('title')
<h2>Show Peminjam {{$peminjams->id}}</h2>
@endsection

@section('content')
<h4> Biodata Peminjam {{$peminjams->id}} </h4>
<p>nama : {{$peminjams->nama_peminjam}}</p>
<p>no HP : {{$peminjams->no_hp_peminjam}}</p>
<p>email : {{$peminjams->email_peminjam}}</p>
@endsection