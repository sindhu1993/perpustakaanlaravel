@extends('adminlte.master')

@section('title')
Edit Peminjam {{$peminjams->id}}
@endsection

@section('content')
<div> 
        <form action="/admin/peminjam/{{$peminjams->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama_peminjam" value="{{$peminjams->nama_peminjam}}" id="nama_peminjam" placeholder="Masukkan Nama">
                @error('nama_peminjam')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">No HP</label>
                <input type="text" class="form-control" name="no_hp_peminjam" value="{{$peminjams->no_hp_peminjam}}" id="no_hp_peminjam" placeholder="Masukkan No Hp">
                @error('no_hp_peminjam')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Email</label>
                <input type="text" class="form-control" name="email_peminjam" value="{{$peminjams->email_peminjam}}" id="email_peminjam" placeholder="Masukkan Email">
                @error('email_peminjam')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection