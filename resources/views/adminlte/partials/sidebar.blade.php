<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ asset('backend/index3.html')}}" class="brand-link">
      <img src="{{ asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">PerpustakaanSABI</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block"> {{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-header">MASTER DATA</li>
          <li class="nav-item">
            <a href="{{ route('/admin/kategori') }}" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Kategori Buku
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('/admin/buku/') }}" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Buku
              </p>
            </a>
          </li>
          <a href="" class="nav-link">
          <i class="fas fa-list-ol"></i>
              <p>
                Kategori
              </p>
            </a>
          </li>
          <li class="nav-item">
<<<<<<< HEAD
            <a href="{{route('/admin/petugas')}}" class="nav-link">
              <i class="nav-icon far fa-calendar-alt"></i>
=======
            <a href="" class="nav-link">
            <i class="fas fa-user-friends"></i>
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
              <p>
                Petugas
              </p>
            </a>
          </li>
          <li class="nav-item">
<<<<<<< HEAD
            <a href="{{route('/admin/peminjam')}}" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
=======
            <a href="../admin/peminjam" class="nav-link">
            <i class="fas fa-users"></i>
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
              <p>
                Peminjam
              </p>
            </a>
          </li>

          <li class="nav-header">TRANSAKSI</li>
          <li class="nav-item">
<<<<<<< HEAD
            <a href="{{ route('/admin/pinjam/') }}" class="nav-link">
              <i class="fa fa-address-book-o"></i>
=======
            <a href="" class="nav-link">
            <i class="fas fa-bookmark"></i>
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
              <p>Peminjaman</p>
            </a>
          </li>
          <li class="nav-item">
<<<<<<< HEAD
            <a href="{{ route('/admin/kembali/') }}" class="nav-link">
              <i class="far fa fa-calendar-check-o"></i>
=======
            <a href="" class="nav-link">
            <i class="fas fa-clipboard-check"></i>
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
              <p>Pengembalian</p>
            </a>
          </li>



        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
