<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiPengembalianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pengembalian', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_transaksi', 100);
            $table->date('tgl_kembali');
            $table->bigInteger('jumlah_buku');
            $table->bigInteger('denda');
            $table->string('status_hapus', 12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pengembalian');
    }
}
