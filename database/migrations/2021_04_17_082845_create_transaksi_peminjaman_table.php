<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiPeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_peminjaman', function (Blueprint $table) {
            $table->bigIncrements('id_transaksi_peminjaman');
            $table->unsignedBigInteger('id_peminjam');
            $table->foreign('id_peminjam')->references('id')->on('peminjam');
            $table->unsignedBigInteger('id_petugas');
            $table->foreign('id_petugas')->references('id')->on('peminjam');
            $table->unsignedBigInteger('id_buku');
            $table->foreign('id_buku')->references('id')->on('peminjam');
            $table->string('kode_trans',100);
            $table->date('tgl_pinjam');
            $table->integer('jumlah_buku');
            $table->string('status_peminjaman',100);
            $table->string('status_hapus',12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_peminjaman');
    }
}
