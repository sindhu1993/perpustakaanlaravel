<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin','DashboardController@index');

// crud Kategori
Route::get('/admin/kategori','KategoriController@index');
Route::post('/admin/kategori', ['as'=>'/admin/kategori','uses'=>'KategoriController@store']);
Route::post('/admin/kategori/edit', ['as'=>'/admin/kategori/edit','uses'=>'KategoriController@prosesEdit']);
Route::post('/admin/kategori/prosesDelete', ['as'=>'admin/kategori/prosesDelete','uses'=>'KategoriController@prosesDelete']);
// crud Kategori

// crud Buku
Route::get('/admin/buku', ['as'=>'/admin/buku/','uses'=>'BukuController@index']);
Route::post('/admin/buku', ['as'=>'/admin/buku','uses'=>'BukuController@store']);
Route::get('/admin/buku/edit/{id_buku}', ['as'=>'/admin/buku/edit/{id_buku}','uses'=>'BukuController@showEdit']);
Route::post('/admin/buku/edit', ['as'=>'/admin/buku/edit','uses'=>'BukuController@prosesEdit']);
Route::post('/admin/buku/prosesDelete', ['as'=>'admin/buku/prosesDelete','uses'=>'BukuController@prosesDelete']);
// crud Buku

// crud pinjam buku
Route::get('/admin/pinjam', ['as'=>'/admin/pinjam/','uses'=>'TransaksiPeminjamanController@index']);
Route::get('/admin/tambahpinjam', ['as'=>'/admin/tambahpinjam/','uses'=>'TransaksiPeminjamanController@addPinjam']);
Route::post('/admin/pinjam/prosesAdd', ['as'=>'/admin/pinjam/prosesAdd','uses'=>'TransaksiPeminjamanController@prosesAdd']);
Route::post('/admin/pinjam/prosesDelete', ['as'=>'admin/pinjam/prosesDelete','uses'=>'TransaksiPeminjamanController@prosesDelete']);
Route::get('/admin/pinjam/edit/{id}', ['as'=>'/admin/pinjam/edit/{id}','uses'=>'TransaksiPeminjamanController@showEdit']);
Route::post('/admin/pinjam/prosesEdit', ['as'=>'/admin/pinjam/prosesEdit','uses'=>'TransaksiPeminjamanController@prosesEdit']);
// crud pinjam buku

// crud kembali buku
Route::get('/admin/kembali', ['as'=>'/admin/kembali/','uses'=>'TransaksiPengembalianController@index']);
Route::get('/admin/tambahkembali', ['as'=>'/admin/tambahkembali/','uses'=>'TransaksiPengembalianController@addKembali']);
Route::post('/admin/kembali/prosesKembali', ['as'=>'/admin/kembali/prosesKembali','uses'=>'TransaksiPengembalianController@prosesKembali']);
Route::get('/admin/kembali/edit/{id}', ['as'=>'/admin/kembali/edit/{id}','uses'=>'TransaksiPengembalianController@showEdit']);
Route::post('/admin/kembali/prosesEdit', ['as'=>'/admin/kembali/prosesEdit','uses'=>'TransaksiPengembalianController@prosesEdit']);
Route::post('/admin/kembali/cari', ['as'=>'/admin/kembali/cari','uses'=>'TransaksiPengembalianController@cari']);
// crud kembali buku

// crud Petugas
Route::get('/admin/petugas','PetugasController@index');
Route::post('/admin/petugas', ['as'=>'/admin/petugas','uses'=>'PetugasController@store']);
Route::post('/admin/petugas/edit', ['as'=>'/admin/petugas/edit','uses'=>'PetugasController@prosesEdit']);
Route::post('/admin/petugas/prosesDelete', ['as'=>'admin/petugas/prosesDelete','uses'=>'PetugasController@prosesDelete']);
// crud Petugas

// crud Peminjam
Route::get('/admin/peminjam','PeminjamController@index');
Route::post('/admin/peminjam', ['as'=>'/admin/peminjam','uses'=>'PeminjamController@store']);
Route::post('/admin/peminjam/edit', ['as'=>'/admin/peminjam/edit','uses'=>'PeminjamController@prosesEdit']);
Route::post('/admin/peminjam/prosesDelete', ['as'=>'admin/peminjam/prosesDelete','uses'=>'PeminjamController@prosesDelete']);
// crud Peminjam

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
