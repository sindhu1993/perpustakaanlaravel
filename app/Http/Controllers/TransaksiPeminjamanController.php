<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TransaksiPeminjamanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $buku = DB::select("select * from buku where status_hapus = 'N' AND stok_buku > 0");
        $peminjam = DB::select("select * from peminjam where status_hapus = 'N'");
        $pinjam = DB::select("SELECT p.kode_trans, b.judul, p.jumlah_buku, p.tgl_pinjam, pem.nama_peminjam, p.id_transaksi_peminjaman
        FROM `transaksi_peminjaman` as p
        LEFT JOIN buku as b ON p.id_buku = b.id
        LEFT JOIN petugas as pet on p.id_petugas = pet.id
        LEFT JOIN peminjam as pem on p.id_peminjam = pem.id
        where p.status_hapus = 'N' AND p.status_peminjaman = 'Dipinjam'");
        return view('adminlte.transaksi_peminjaman.table',['peminjam'=>$peminjam,'buku'=>$buku,'pinjam'=>$pinjam]);
    }

    public function addPinjam()
    {
        $buku = DB::select("select * from buku where status_hapus = 'N' AND stok_buku > 0");
        $peminjam = DB::select("select * from peminjam where status_hapus = 'N'");
        $k = DB::select('select max(id_transaksi_peminjaman) + 1 as no_urut from transaksi_peminjaman where status_hapus = ?', ['N']);
        $kode_trans = "P".$k[0]->no_urut;
        return view('adminlte.transaksi_peminjaman.add',['peminjam'=>$peminjam,'buku'=>$buku,'kode'=>$kode_trans]);
    }

    public function prosesAdd(Request $request)
    {
        // dd($request->all());
        $tgl_pinjam = $request->tgl_pinjam;
        $ubah_tgl_pinjam = date("Y-m-d", strtotime($tgl_pinjam));
        // $id_petugas = 1;

        $id_petugas = Auth::id();



        $jml_buku = $request->jumlah_buku;

        $i = DB::insert('insert into transaksi_peminjaman (kode_trans, id_buku, id_peminjam, jumlah_buku, tgl_pinjam, id_petugas, status_hapus, status_peminjaman) values (?, ?,?,?,?,?,?,?)', [$request->kode_trans, $request->id_buku, $request->id_peminjam, $request->jumlah_buku, $ubah_tgl_pinjam, $id_petugas, 'N','Dipinjam']);

        $u = DB::update("update buku set stok_buku = stok_buku - $jml_buku where id = ?", [$request->id_buku]);
        return redirect('/admin/pinjam')->with('success','Success Add Data');
    }

    public function showEdit($id)
    {
        $buku = DB::select("select * from buku where status_hapus = 'N' AND stok_buku > 0");
        $peminjam = DB::select("select * from peminjam where status_hapus = 'N'");

        $tx = DB::select("SELECT p.kode_trans, b.judul, p.jumlah_buku, date_format(p.tgl_pinjam,'%d-%m-%Y') as tgl_pinjam  ,
        pem.nama_peminjam, p.id_transaksi_peminjaman, p.id_buku, p.id_peminjam
        FROM `transaksi_peminjaman` as p
        LEFT JOIN buku as b ON p.id_buku = b.id
        LEFT JOIN petugas as pet on p.id_petugas = pet.id
        LEFT JOIN peminjam as pem on p.id_peminjam = pem.id
        where p.status_hapus = 'N' and p.id_transaksi_peminjaman = $id");

        return view('adminlte.transaksi_peminjaman.edit',['peminjam'=>$peminjam,'buku'=>$buku,'pinjam'=>$tx]);
    }

    public function prosesEdit(Request $request)
    {
        // dd($request->all());

        $tgl_pinjam = $request->tgl_pinjam;
        $ubah_tgl_pinjam = date("Y-m-d", strtotime($tgl_pinjam));
        // $id_petugas = 1;
        $id_petugas = Auth::id();

        $jml_buku_baru = $request->jumlah_buku;

        $old = DB::select('select * from transaksi_peminjaman where id_transaksi_peminjaman = ?', [$request->id_transaksi_peminjaman]);

        if($old[0]->jumlah_buku == $jml_buku_baru)
        {
            // tidak update stok buku
            $u = DB::update("update transaksi_peminjaman set
            id_buku = ?,
            id_peminjam = ?,
            jumlah_buku = ?,
            tgl_pinjam = ?,
            id_petugas = ?
            where id_transaksi_peminjaman = ?
            ", [$request->id_buku, $request->id_peminjam, $request->jumlah_buku,$ubah_tgl_pinjam, $id_petugas,$request->id_transaksi_peminjaman]);

            return redirect('/admin/pinjam')->with('success','Success Edit Data');

            // echo "tidak update stok";
        }

        else
        {
            $old_stok = DB::select('select * from buku where id = ?', [$request->id_buku]);
            $stok_awal = (int) $old[0]->jumlah_buku + (int) $old_stok[0]->stok_buku;
            $update_stok = (int) $stok_awal - (int) $request->jumlah_buku;

            // echo $update_stok;

            // update stok buku
            $tx = DB::update("update transaksi_peminjaman set
            id_buku = ?,
            id_peminjam = ?,
            jumlah_buku = ?,
            tgl_pinjam = ?
            where id_transaksi_peminjaman = ?
            ", [$request->id_buku, $request->id_peminjam, $request->jumlah_buku,$ubah_tgl_pinjam, $request->id_transaksi_peminjaman]);

            $u = DB::update("update buku set stok_buku = $update_stok where id = ?", [$request->id_buku]);

            return redirect('/admin/pinjam')->with('success','Success Edit Data');

            // echo "update stok buku";
        }
    }

    public function prosesDelete(Request $request)
    {
        // dd($request->all());
        DB::update("update transaksi_peminjaman set status_hapus = 'Y' where id_transaksi_peminjaman = ?", [$request->id_transaksi_peminjaman]);
        return redirect('/admin/pinjam')->with('success','Success Add Data');
    }
}
