<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeminjamController extends Controller
{
    //
    public function index()
    {
        $peminjam = DB::select('select * from peminjam where status_hapus = :status_hapus', ['status_hapus' => 'N']);
        return view('adminlte.peminjam.table',compact('peminjam'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // dd($request->all());

        $k = DB::insert('insert into peminjam (nama_peminjam, email_peminjam,no_hp_peminjam, status_hapus)
        values (?, ?,?,?)', [$request->nama_peminjam, $request->email_peminjam,$request->no_hp_peminjam,'N']);

        return redirect('/admin/peminjam')->with('success','Success Add Data');
    }

    public function prosesEdit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // dd($request->all());
        $query = DB::table('peminjam')
        ->where('id',$request->id)
        ->update([
            "nama_peminjam" => $request['nama_peminjam'],
            "email_peminjam" => $request['email_peminjam'],
            "no_hp_peminjam" => $request['no_hp_peminjam']
        ]);

        return redirect('/admin/peminjam')->with('success','Success Edit Data');
    }

    public function prosesDelete(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $check_data = DB::table('peminjam')->where('id',$request->id)->count();
        if($check_data > 0)
        {
            $peminjam = [
                'status_hapus' => 'Y',
            ];

            DB::table('peminjam')->where('id',$request->id)->update($peminjam);

            return redirect('/admin/peminjam')->with('success','Success Delete Data');
        }
    }
}
