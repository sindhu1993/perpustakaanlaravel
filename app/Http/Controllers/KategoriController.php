<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $kategori_buku = DB::select('select * from kategori_buku where status_hapus = :status_hapus', ['status_hapus' => 'N']);
        return view('adminlte.kategori.table',compact('kategori_buku'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // dd($request->all());

        $k = DB::insert('insert into kategori_buku (nama_kategori, status_hapus)
        values (?, ?)', [$request->nama_kategori, 'N']);

        return redirect('/admin/kategori')->with('success','Success Add Data');
    }

    public function prosesEdit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // dd($request->all());
        $query = DB::table('kategori_buku')
        ->where('id',$request->id)
        ->update([
            "nama_kategori" => $request['nama_kategori'],
        ]);

        return redirect('/admin/kategori')->with('success','Success Edit Data');
    }

    public function prosesDelete(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $check_data = DB::table('kategori_buku')->where('id',$request->id)->count();
        if($check_data > 0)
        {
            $kategori_buku = [
                'status_hapus' => 'Y',
            ];

            DB::table('kategori_buku')->where('id',$request->id)->update($kategori_buku);

            return redirect('/admin/kategori')->with('success','Success Delete Data');
        }
    }
}
