<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
<<<<<<< HEAD
use Illuminate\Support\Facades\DB;

class PetugasController extends Controller
{
    //
=======

class PetugasController extends Controller
{
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $petugas = DB::select('select * from petugas where status_hapus = :status_hapus', ['status_hapus' => 'N']);
        return view('adminlte.petugas.table',compact('petugas'));
    }

    public function store(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // dd($request->all());

        $k = DB::insert('insert into petugas (nama_petugas, email_petugas,no_hp_petugas, status_hapus) 
        values (?, ?,?,?)', [$request->nama_petugas, $request->email_petugas,$request->no_hp_petugas,'N']);
        
        return redirect('/admin/petugas')->with('success','Success Add Data');
    }

    public function prosesEdit(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        // dd($request->all());
        $query = DB::table('petugas')
        ->where('id',$request->id)
        ->update([
            "nama_petugas" => $request['nama_petugas'],
            "email_petugas" => $request['email_petugas'],
            "no_hp_petugas" => $request['no_hp_petugas']
        ]);

        return redirect('/admin/petugas')->with('success','Success Edit Data');
    }

    public function prosesDelete(Request $request)
    {
        date_default_timezone_set('Asia/Jakarta');
        $check_data = DB::table('petugas')->where('id',$request->id)->count();
        if($check_data > 0)
        {
            $petugas = [
                'status_hapus' => 'Y',
            ];

            DB::table('petugas')->where('id',$request->id)->update($petugas);

            return redirect('/admin/petugas')->with('success','Success Delete Data');
        }
    }
}
