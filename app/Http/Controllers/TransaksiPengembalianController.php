<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
<<<<<<< HEAD
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TransaksiPengembalianController extends Controller
{
    //
=======

class TransaksiPengembalianController extends Controller
{
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $buku = DB::select("select * from buku where status_hapus = 'N' AND stok_buku > 0");
        $peminjam = DB::select("select * from peminjam where status_hapus = 'N'");
        $kembali = DB::select("SELECT p.kode_trans, b.judul, p.jumlah_buku, p.tgl_kembali, pem.nama_peminjam, p.id_transaksi_pengembalian,
        p.denda
        FROM `transaksi_pengembalian` as p
        LEFT JOIN buku as b ON p.id_buku = b.id
        LEFT JOIN petugas as pet on p.id_petugas = pet.id
        LEFT JOIN peminjam as pem on p.id_peminjam = pem.id
        where p.status_hapus = 'N' AND p.status_pengembalian = 'Dikembalikan'");
<<<<<<< HEAD
        return view('adminlte.transaksi_pengembalian.table',['peminjam'=>$peminjam,'buku'=>$buku,'kembali'=>$kembali]);
=======
        return view('adminlte.transaksi_pengembalian.index',['peminjam'=>$peminjam,'buku'=>$buku,'kembali'=>$kembali]);
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
    }

    public function addKembali()
    {
<<<<<<< HEAD
        return view('adminlte.transaksi_pengembalian.add');
=======
        return view('adminlte.transaksi_pengembalian.create');
>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
    }

    public function cari(Request $request)
    {

        // var_dump($request->all());
        $x = DB::select("SELECT p.kode_trans, b.judul, p.jumlah_buku, p.tgl_pinjam, pem.nama_peminjam, p.id_transaksi_peminjaman,
        p.id_buku
        FROM `transaksi_peminjaman` as p
        LEFT JOIN buku as b ON p.id_buku = b.id
        LEFT JOIN petugas as pet on p.id_petugas = pet.id
        LEFT JOIN peminjam as pem on p.id_peminjam = pem.id
        where p.status_hapus = 'N' AND p.status_peminjaman = 'Dipinjam'
        AND p.kode_trans = '$request->kode_trans'");

        if(count($x) > 0)
        {
            echo json_encode(array('status'=>1,'data'=>$x));
        }

        else
        {
            echo json_encode(array('status'=>9,'message'=> 'Data Tidak Ada'));
        }
    }

    public function prosesKembali(Request $request)
    {
        // dd($request->all());

        $tgl_kembali = $request->tgl_kembali;
        $ubah_tgl_kembali = date("Y-m-d", strtotime($tgl_kembali));
        // $id_petugas = 1;

        $id_petugas = Auth::id();

        $denda = 0;

        if($request->denda !== "")
        {
            $denda = $request->denda;
        }

        $old_stok = DB::select('select * from buku where id = ?', [$request->id_buku]);
        $stok_sekarang = (int) $request->jumlah_buku + (int) $old_stok[0]->stok_buku;

        $i = DB::insert("INSERT INTO transaksi_pengembalian (id_peminjam, id_petugas, id_buku, kode_trans, tgl_kembali,
        jumlah_buku, denda,status_hapus, status_pengembalian)
        SELECT id_peminjam, '$id_petugas', id_buku, kode_trans, '$ubah_tgl_kembali', jumlah_buku, $denda, 'N', 'Dikembalikan'
        FROM transaksi_peminjaman
        WHERE kode_trans = '$request->kode_trans'");

        $u = DB::update("update buku set stok_buku = $stok_sekarang where id = ?", [$request->id_buku]);

        $ux = DB::update("update transaksi_peminjaman set status_peminjaman = 'Dikembalikan' where kode_trans = ?", [$request->kode_trans]);


        return redirect('/admin/kembali')->with('success','Success Add Data');

        // $sql = "INSERT INTO transaksi_pengembalian (id_peminjam, id_petugas, id_buku, kode_trans, tgl_kembali,
        // jumlah_buku, denda)
        // SELECT id_peminjam, '$id_petugas', id_buku, kode_trans, '$ubah_tgl_kembali', jumlah_buku, $denda
        // FROM transaksi_peminjaman
        // WHERE kode_trans = '$request->kode_trans'";
        // echo $sql;
    }

    public function showEdit($id)
    {
        // dd($id);
        $buku = DB::select("select * from buku where status_hapus = 'N' AND stok_buku > 0");
        $peminjam = DB::select("select * from peminjam where status_hapus = 'N'");

        $kembali = DB::select("SELECT p.kode_trans, b.judul, p.jumlah_buku, 
        date_format(p.tgl_kembali,'%d-%m-%Y') as tgl_kembali , pem.nama_peminjam, p.id_transaksi_pengembalian,
        p.id_buku, p.denda
        FROM `transaksi_pengembalian` as p
        LEFT JOIN buku as b ON p.id_buku = b.id
        LEFT JOIN petugas as pet on p.id_petugas = pet.id
        LEFT JOIN peminjam as pem on p.id_peminjam = pem.id
        where p.status_hapus = 'N' AND p.status_pengembalian = 'Dikembalikan'
        AND p.id_transaksi_pengembalian = $id");

        return view('adminlte.transaksi_pengembalian.edit',['peminjam'=>$peminjam,'buku'=>$buku,'kembali'=>$kembali]);
    }

    public function prosesEdit(Request $request)
    {
        // dd($request->all());

        $tgl_kembali = $request->tgl_kembali;
        $ubah_tgl_kembali = date("Y-m-d", strtotime($tgl_kembali));
        // $id_petugas = 1;

        $id_petugas = Auth::id();

        $denda = 0;

        if($request->denda !== "")
        {
            $denda = $request->denda;
        }

        $d = DB::update("update transaksi_pengembalian set 
        denda = $denda, tgl_kembali = '$ubah_tgl_kembali', id_petugas = $id_petugas
        where id_transaksi_pengembalian = ?", [$request->id_transaksi_pengembalian]);

        return redirect('/admin/kembali')->with('success','Success Edit Data');
    }
<<<<<<< HEAD
=======

>>>>>>> 3319d2bcf5e9ef0802ee33c03ab24fadb66044e6
}
