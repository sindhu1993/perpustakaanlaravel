<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $kategori = DB::select("select * from kategori_buku where status_hapus = 'N'");
        $buku = DB::select("select b.id,k.nama_kategori, b.cover_buku, b.judul, b.pengarang,
        b.tahun_terbit, b.stok_buku, b.tempat_simpan
        from buku as b
        left join kategori_buku as k
        on b.id_kategori = k.id
        where b.status_hapus = 'N'");
        return view('adminlte.buku.table',['kategori'=>$kategori,'buku'=>$buku]);
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $gambar = $request->cover_buku;
        $new_gambar = time().$gambar->getClientOriginalName();

        $k = DB::insert('insert into buku (cover_buku, judul, pengarang,
        tahun_terbit, stok_buku, tempat_simpan, status_hapus, id_kategori)
        values (?, ?,?,?,?,?,?,?)',
        [$new_gambar, $request->judul, $request->pengarang,
        $request->tahun_terbit, $request->stok_buku, $request->tempat_simpan,
        'N', $request->id_kategori]);

        $gambar->move('uploads/buku/', $new_gambar);
        return redirect('/admin/buku')->with('success','Success Add Data');
    }

    public function prosesDelete(Request $request)
    {
        // dd($request->all());

        $k = DB::update('update buku set
            status_hapus = ?
            where id = ?',
            ['Y', $request->id]);

        return redirect('/admin/buku')->with('success','Success Delete Data');
    }

    public function showEdit($id)
    {
        $kategori = DB::select("select * from kategori_buku where status_hapus = 'N'");
        $buku = DB::select("select b.id,k.nama_kategori, b.cover_buku, b.judul, b.pengarang,
        b.tahun_terbit, b.stok_buku, b.tempat_simpan, b.id_kategori
        from buku as b
        left join kategori_buku as k
        on b.id_kategori = k.id
        where b.id = '$id'");
        return view('adminlte.buku.edit',['kategori'=>$kategori,'buku'=>$buku]);
    }

    public function prosesEdit(Request $request)
    {
        // edit with move uplaod
        if($request->cover_buku !== null)
        {
            $old_image = $request->old_image;
            unlink('uploads/buku/'.$old_image);

            $img = $request->cover_buku;
            $img_new = time().$img->getClientOriginalName();

            $k = DB::update('update buku set
            cover_buku = ?, judul = ?, pengarang = ?, tahun_terbit = ?, stok_buku = ?,
            tempat_simpan = ?
            where id = ?',
            [$img_new,$request->judul,$request->pengarang, $request->tahun_terbit,
            $request->stok_buku, $request->tempat_simpan, $request->id]);

            // move upload file
            $img->move('uploads/buku/',$img_new);
            return redirect('/admin/buku')->with('success','Success Edit Data');
        }

        else
        {
            $old_image = $request->old_image;
            $k = DB::update('update buku set
                cover_buku = ?, judul = ?, pengarang = ?, tahun_terbit = ?, stok_buku = ?,
                tempat_simpan = ?
                where id = ?',
                [$old_image,$request->judul,$request->pengarang, $request->tahun_terbit,
                $request->stok_buku, $request->tempat_simpan, $request->id]);

                return redirect('/admin/buku')->with('success','Success Edit Data');
        }
    }
}
